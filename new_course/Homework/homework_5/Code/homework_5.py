import networkx as nx
import csv
import numpy as np
import pandas as pd
import itertools
import random
import pickle
from sklearn.metrics import roc_curve, auc
import time
import multiprocessing
from sklearn.metrics import roc_auc_score



def get_norwegian_dataset():
    G = nx.read_edgelist("./Datasets/net1m_2011-08-01.txt")
    metadata = []
    with open('./Datasets/metadata_norwegians.txt', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            metadata.append(row[0].split('"'))
    clean_metadata  = []
    for row in metadata:
        new_row = []
        for i in row:
            a = i.strip()
            new_row.append(a)
        clean_metadata.append(new_row)
    del clean_metadata[0]
    for node in G.nodes():
        attributes = clean_metadata[int(node) - 1]
        G.node[attributes[0]]['Name'] = attributes[1]
        G.node[attributes[0]]['gender'] = int(attributes[2])
    return G


def get_hvr_dataset():
    G = nx.read_edgelist('./Datasets/HVR_5.txt', delimiter=',')
    metadata_HVR = []
    with open("Datasets/metadata_CysPoLV.txt", "r") as f:
        reader = csv.reader(f)
        for row in reader:
            metadata_HVR.append(row[0])
    for i, val in enumerate(metadata_HVR):
        try:
            G.node[str(i + 1)]['attribute'] = val
        except KeyError:
            continue
    return G


def obscure_edges(G, frac_to_keep):
    orig_edges = list(G.edges())
    frac_to_remove = 1 - frac_to_keep
    num_edges_to_remove = int(np.ceil(frac_to_remove * G.number_of_edges()))
    random.shuffle(orig_edges)
    F = G.copy()
    for i in range(num_edges_to_remove):
        F.remove_edge(orig_edges[i][0], orig_edges[i][1])
    return F


def degree_product_score(F, G):
    nodelist = list(G.nodes())
    n = len(nodelist)
    potential_edges = list(itertools.combinations(nodelist, 2))
    degree_product_list = []
    for edge in potential_edges:
        # This should filter out edges that we know
        if edge in F.edges():
            continue
        else:
            deg1 = F.degree(edge[0])
            deg2 = F.degree(edge[1])
            if edge in G.edges():
                truth_value = 1
            else:
                truth_value = 0
            degree_product_list.append([edge, (deg1 * deg2) + (float(random.random()) / float(n)), truth_value])
    return degree_product_list


def normalized_common_neighbors_score(F, G):
    nodelist = list(G.nodes())
    n = len(nodelist)
    potential_edges = list(itertools.combinations(nodelist, 2))
    normalized_common_neighbor_list = []
    for edge in potential_edges:
        # This should filter out edges that we know
        if edge in F.edges():
            continue
        else:
            neighbors_1 = list(F.neighbors(edge[0]))
            neighbors_2 = list(F.neighbors(edge[1]))
            intersection = set(neighbors_1).intersection(neighbors_2)
            union = set(neighbors_1).union(neighbors_2)
            try:
                score = float(len(intersection))/float(len(union)) + random.random()/n
            except ZeroDivisionError:
                score = 0

            if edge in G.edges():
                truth_value = 1
            else:
                truth_value =0
            normalized_common_neighbor_list.append([edge, score, truth_value])
    return normalized_common_neighbor_list


def shortest_path_score(F, G):
    nodelist = list(G.nodes())
    n = len(nodelist)
    potential_edges = list(itertools.combinations(nodelist, 2))
    shortest_path_score_list = []
    for edge in potential_edges:
        # This should filter out edges that we know
        if edge in F.edges():
            continue
        else:
            try:
                length = nx.shortest_path_length(F, edge[0], edge[1])
                tmp_score = float(1)/float(length)
                score = tmp_score + float(random.random())/float(n)
    #             print(score == tmp_score)
                if edge in G.edges():
                    truth_value = 1
                else:
                    truth_value = 0
                shortest_path_score_list.append([edge, score, truth_value])
            except nx.NetworkXNoPath:
                continue
    return shortest_path_score_list


def create_dataframe(score_list, F, G):
    final_list = []
    for item in score_list:
        # Get rid of known values
        if item[0] in F.edges():
            continue
        # record Values which are not in F, but where in original
        if item[0] in G.edges():
            final_list.append([item[0], item[1], 1])
        else:
            final_list.append([item[0], item[1], 0])
    df = pd.DataFrame(final_list)
    df.sort_values(1, ascending=False ,inplace=True)
    df.reset_index(drop=True)
    return df


def get_yvals_from_datalist(data_list):
    # Input: a list of data in the format [edge_tuple, edge_score, truth_value]
    # Truth value is 1 if the edge is supposed to exist.

    # This dataframe work is just remnants from working in jupyter... (Ideally, would never convert away from lists.
    df = pd.DataFrame(data_list)
    df.sort_values(1, inplace=True, ascending=False)
    df.reset_index(inplace=True, drop=True)
    df[1] = df[1] / df[1][0] # Normalize the values (Necessary in the case of degree score)

    y_truth = []
    y_score = []

    for row in df.itertuples():
        y_truth.append(row[3])
        y_score.append(row[2])

    #
    # for row in df.itertuples():
    #     if row[3] == 1:
    #         y_truth.append([0, 1])
    #     else:
    #         y_truth.append([1, 0])
    #     y_score.append([1 - row[2], row[2]])
    #
    # # Convert to datatype consistent with ROC function.
    # y_truth = np.array(y_truth)
    # y_score = np.array(y_score)
    return y_truth, y_score


def calc_roc_auc_accuracy(y_truth, y_score):
    try:
        score = roc_auc_score(y_truth, y_score)
        return score
    except ValueError:
        return -1


def Main_Multiprocessing(args):
    fraction_observed = args[0]
    iterations = args[1]
    dataset = args[2]

    if dataset == 'hvr':
        G = get_hvr_dataset()
    elif dataset == 'norwegians':
        G = get_norwegian_dataset()
    else:
        raise ValueError("Please select 'hvr' or 'norwegians' as input")

    degree_step_accuracies = []
    normal_step_accuracies = []
    shortest_step_accuracies = []

    for j in range(iterations):
        # F, removed_edges = obscure_edges(G, i)
        F = obscure_edges(G, fraction_observed)
        degree_scores_list = degree_product_score(F, G)
        normal_scores_list = normalized_common_neighbors_score(F, G)
        shortest_scores_list = shortest_path_score(F, G)

        # The roc function requires a set of classification scores and truth values
        y_truth_degree, y_score_degree = get_yvals_from_datalist(degree_scores_list)
        y_truth_normal, y_score_normal = get_yvals_from_datalist(normal_scores_list)
        y_truth_shortest, y_score_shortest = get_yvals_from_datalist(shortest_scores_list)

        # Collect Accuracies for this iteration/observed fraction
        degree_score = calc_roc_auc_accuracy(y_truth_degree, y_score_degree)
        normal_score = calc_roc_auc_accuracy(y_truth_normal, y_score_normal)
        shortest_score = calc_roc_auc_accuracy(y_truth_shortest, y_score_shortest)

        # Store the accuracy at each iteration
        if degree_score != -1:
            degree_step_accuracies.append(degree_score)
        if normal_score != -1:
            normal_step_accuracies.append(normal_score)
        if shortest_score != -1:
            shortest_step_accuracies.append(shortest_score)

    # Convert lists of accuracies to array for easy mean computation.
    degree_step_accuracies = np.array(degree_step_accuracies)
    normal_step_accuracies = np.array(normal_step_accuracies)
    shortest_step_accuracies = np.array(shortest_step_accuracies)

    final_degree_accuracy = degree_step_accuracies.mean()
    final_normal_accuracy = normal_step_accuracies.mean()
    final_shortest_accuracy = shortest_step_accuracies.mean()

    return final_degree_accuracy, final_normal_accuracy, final_shortest_accuracy, fraction_observed


if __name__ == "__main__":
    toc = time.time()
    observed_granularity = 15
    observed_values = [float(i)/float(observed_granularity) for i in range(1, observed_granularity + 1)]
    iterations = 20
    dataset = 'norwegians'

    args = [[value, iterations, dataset] for value in observed_values]

    pool = multiprocessing.Pool()
    results = pool.map(Main_Multiprocessing, args)

    with open('all_results_norwegians_big_20.pkl', 'wb') as f:
        pickle.dump(results, f)
    tic = time.time()
    print(tic - toc)




    # toc = time.time()
    # observed_granularity = 30
    # observed_values = [float(i)/float(observed_granularity) for i in range(1, observed_granularity + 1)]
    # iterations = 20
    # dataset = 'hvr'
    #
    # args = [[value, iterations, dataset] for value in observed_values]
    #
    # pool = multiprocessing.Pool()
    # results = pool.map(Main_Multiprocessing, args)
    #
    # with open('all_results_hvr_big_20.pkl', 'wb') as f:
    #     pickle.dump(results, f)
    # tic = time.time()
    # print(tic - toc)