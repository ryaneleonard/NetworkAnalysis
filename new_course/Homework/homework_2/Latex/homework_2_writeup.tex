\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ (\hmwkClassInstructor\ \hmwkClassTime): \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework\ \#1}
\newcommand{\hmwkDueDate}{September 12}
\newcommand{\hmwkClass}{Network Analysis}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{Aaron Clauset}
\newcommand{\hmwkAuthorName}{\textbf{Ryan Leonard} }

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 11:59pm}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak

% Problem 1
\begin{homeworkProblem}
	Consider the following and rather unrealistic model of a network: each of n vertices belongs to one of g groups. The mth group has $n_m$ vertices and each vertex in that group is connected to the others in the group with independent probability $ p_m = A(n_m - 1 ) ^{-\beta}$ Where A and $\beta$ are constants, but not to any vertices in other groups. Thus, this network takes the form of a set of disjoint groups of communities\\
	\textbf{a)}: Calculate the expected degree $\langle k \rangle$ of a vertex in group m.
	\begin{align*}
	\langle k \rangle &= \frac{2}{n}\binom{n}{2} p = (n-1)p\\	
	\langle k_m \rangle &= \frac{2}{n_m}\binom{n_m}{2} p_m = (n_m-1)p_m\\
	&= (n_m - 1)A(n_m - 1) ^{-\beta}\\
	&= A(n_m -1)^{1- \beta}
	\end{align*}
	\\
	
	
	\textbf{b)}: Calculate the expected value $\langle C_m \rangle$ of the local clustering coefficient for vertices in group m.\\
	\begin{align*}
	C &= \frac{\langle k \rangle}{n_m - 1}\\
	&= \frac{A(n_m - 1)^{1-\beta}}{(n_m -1)}\\
	&= A(n_m - 1)^{-\beta}
	\end{align*}
	
	
	\textbf{c)}: Hence, show that $\langle C_m \rangle \propto \langle k \rangle ^{-\beta / (1 - \beta)}$. What value would $\beta$ have to assume for the expected value of the local clustering coefficient to fall off as $\langle k \rangle ^{-0.75}$ as has been conjectured by some researchers?\\
	First, we show that $\langle C_m \rangle \propto \langle k \rangle ^{-\beta/(1-\beta)}$\\
	\begin{align*}
	\langle C_m \rangle &\propto \langle k \rangle ^{-\beta/(1-\beta)}\\
	A(n_m - 1)^{-\beta} &\propto [A(n_m - 1)^{(1 - \beta)}]^{-\beta/(1-\beta)}\\
	A(n_m - 1)^{-\beta} &\propto A(n_m - 1)^{-\beta}
	\end{align*}
	Solving for $\frac{-\beta}{(1-\beta)}  = -0.75$. We see $\beta = \frac{3}{7}$
	

	
\end{homeworkProblem}

% Problem 2

\begin{homeworkProblem}
	Consider the random graph $G(n, p)$ with average degree $c$\\
	\textbf{a)} show that in the limit of large n the expected number of triangles in the network is $\frac{c^3}{6}$. In other words, show that the number of triangles is constant, neither growing nor vanishing in the limit of large n.\\
	\textbf{Solution:}\\
	Citation: http://mae.engr.ucdavis.edu/dsouza/Classes/289-S14/hw2\_soln2014.pdf\\
	
	The number of triangles in a network is equal to the $$ \binom{n}{3} p^3$$ \\
	Broken down, this expression simply translates to the number of ways to choose 3 nodes within the network, and the probability that these nodes are connected. Additionally, the probability that any three nodes are connected can be resolved to $p^3$, as the probability $p$ that any two nodes are connected is iid. \\
	Further evaluating this expression, we have:

	\begin{align*}
	Triangles &= \binom{n}{3} p^3\\
	&=p^3 \frac{n(n-1)(n-2)}{6}\\
	&= \frac{(np)^3 - 3(np)^2p + 2(np)p^2}{6}\\
	\end{align*}
	
	At this point in the calculations, observe that $ c = \frac{2m}{n} $, where the number of edges, $m$ is equal to the number of ways to choose two edges, multiplied by the probability of an edge existing at all, i.e. $m = \binom{n}{2} p$. \\
	
	Thus, through some amount of algebra, we can see that c resolves to: $np - p$. \\
	Because the interest of this problem is in what happens as n approaches infinity, we can drop all terms which lack an $n$, as they will vanish by comparison.\\
	This allows us to make the approximation $c \approx np$\\
	Making this substitution, we continue...
	
	\begin{align*}
	Triangles &\approx \frac{c^3 - 3c^2p + 2cp^2}{6} \\
	& \approx \frac{c^3}{6} \text{ as } n \rightarrow \infty
	\end{align*}

	
	\textbf{b)} Show that the expected number of connected triples in the network, as Eq. (7.41) in Networks, is $ \frac{1}{2} nc^2$\\
	\textbf{Solution:}\\
	As above, because the node connections are iid. The probability of any particular connected triple existing is equal to $p^2$. 
	Thus the problem resolves to how many ways can we connect three nodes given the expected probability $p^2$ (probability of two iid edges existing between three nodes), i.e. $\text{Triples} = 3 \binom{n}{3} p^2$ where the 3 in front accounts for the three ways to connect 3 vertices in an undirected graph (see figure below).\\
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Problem2.jpg}\\
	Simplifying, we see:
	\begin{align*}
	\text{Triples} &= 3 \binom{n}{3} p^2 \\
	&= \frac{3n(n-2)(n-1)}{3!}\\
	&= \frac{n^3 - 3n^2 + 2n}{2} p^2\\
	&= \frac{(np)^2 n- 3(np)^2 + 2(np)p}{2}\\
	&= \frac{c^2 n- 3c^2 + 2cp}{2}\\
	&\approx \frac{1}{2}nc^2 \text{ as } n \rightarrow \infty
	\end{align*}
	
	In the last step, notice that $c$ is a function of $n$. Because we are interested in what happens as $n \rightarrow \infty$, we can approximate using the highest power term as the other terms will become insignificant by comparison.\\
	
	\textbf{c)} Hence, calculate the clustering coefficient $C$, as defined in Eq. (7.41) in Networks, and confirm that it agrees for large n with the value given in Eq. (12.11) in Networks.\\
	\textbf{Solution:}\\
	Equation 7.41 states: 
	$$ C = \frac{ \text{Number of triangles} \times 3}{\text{Numaber of connected triples}}$$
	Equation 12.11 states:
	$$ C = \frac{c}{n-1}$$
	Dividing the solutions from parts \textbf{a} and \textbf{b}, we get:
	$$ \frac{c^3}{6} \frac{6}{nc^2}  = \frac{c}{n}$$
	Thus, this solution agrees with equation for large n. i.e. for both equations, as n approaches infinity, the clustering coefficient converges to 0.
	
	\textbf{Note To Grader:}\\
	On my last homework assignment, I received zero points on a problem for which I cited a solution I found elsewhere. In the feedback I received for that problem, I was told to make it clear why I took the steps I did in order to avoid the appearance of plagiarism. While I maintain the claim that I fully understand the steps taken in all work I submit, there are scenarios where mathematical operations performed can not be modified. I hope that by writing reasoning behind many of the steps taken in this problem helps to prove my understanding, and that I be given the opportunity to prove the honesty of my solutions going forward should questions of plagiarism be raised again.
\end{homeworkProblem}


% Problem 3
\begin{homeworkProblem}
	Consider an undirected, unweighted network of n vertices that contains exactly two subnetworks of size $n_A$ and $n_b$, which are connected by a single edge (A, B). Show that the closeness centralities $C_A$ and $C_B$ of vertices between A and B as defined by Eq. 7.29 in Networks, are related by $$\frac{1}{C_A} + \frac{n_A}{n} = \frac{1}{C_B} + \frac{n_B}{n}$$\\
	\textbf{Solution:}\\
	Eq. 7.29 states: $$C_i = \frac{1}{l_i} = \frac{n}{\sum_j d_{ij}}$$
	
	Substituting $C_A$ and $C_B$ with $\frac{n}{\sum_j d_{Aj}} and \frac{n}{\sum_k d_{Bk}}$, respectively, we have:
	\begin{align*}
	\frac{1}{C_A} + \frac{n_A}{n} &= \frac{1}{C_B} + \frac{n_B}{n}\\
	\frac{\sum_j d_{Aj}}{n}  + \frac{n_A}{n} &= \frac{\sum_k d_{Bk}}{n} + \frac{n_B}{n}\\
	\sum_j d_{Aj} + n_A &= \sum_k d_{Bk} + n_B\\
	\sum_j d_{Aj} - \sum_k d_{Bk} &= n_B - n_A
	\end{align*}
	
	Now, let $\sum_j d_{Aj} = \sum_{j_i} d_{Aj_i} + \sum_{j_o} d_{Aj_o}$, where the $j_i$ represents the nodes within the subcomponent containing the nodes $n_A$, and $j_0$ represents the nodes within the subcomponent $n_B$. (And using similar notation for $\sum_k d_{Bk}$) It can be observed that $\sum_{j_o} d_{Aj_o} = \sum_{k_i} d_{Bk_i} + n_B$ Because incorporating the centrality score of $n_B$ into $C_A$ will require traversing a single edge $n_B$ times.
	
	\begin{align*}
	\sum_j d_{Aj} - \sum_k d_{Bk} &= n_B - n_A\\
	\sum_{j_i} d_{Aj_i} + \sum_{j_o} d_{Aj_o} -[\sum_k d_{Bk_i} + \sum_k d_{Bk_o}] &= n_B - n_A\\
	\sum_{j_i} d_{Aj_i} + \sum_{k_i} d_{Bk_i} + n_B -\sum_{k_i} d_{Bk_i} - \sum_{j_i} d_{Aj_i} - n_A &= n_B - n_A\\
	n_B - n_A = n_B - n_A
	\end{align*}
\end{homeworkProblem}


% Problem 4
\begin{homeworkProblem}
	Consider an undirected (connected) tree of n vertices. Suppose that a particular vertex in the tree has degree k, so that its removal would divide the tree into k disjoint regions, and suppose that the sizes of those regions are $n_1, ..., n_k$. Show that the betweenness centrality b of the vertex is $$b = n^2 - \sum_{i=1}^{k} n_i^2$$\\
	\textbf{Solution:}\\
	Citation: https://github.com/wangjohn/mit-courses/blob/master/14.15/pset1.tex\\
	
	First, observe that we are dealing with a tree, and thus our graph has no self loops, and thus each subregion may only have a single connection to the red node $x_i$. i.e. Edges such as the hollow line connecting nodes 1 and 12 are not allowed. \\
	Using the diagram below as a visual guide, we can see that the removal of the red node will create 3 distinct subregions. \\
	Within each subregion, we observe that there are $n_m^2$ shortest paths, and that the removed node $x_i$ is not included in any of them.\\
	Additionally, we observe that by the definition of the problem, that the only way to connect any two of the disjoint subregions, is to pass through the red node.\\
	Given these observations, we notice that there are $n^2$ shortest paths within any graph tree, and by removing all shortest paths which don't contain $x_i$, i.e. $\sum_{m=1}^{k} n_m^2$, we are left with the centrality score $$x_i = n^2 - \sum_{m=1}^{k} n_m^2$$
	
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Problem4.jpg}\\
	

\textbf{Note to Grader:}\\
Addressing further concerns of plagiarism paranoia, I found this to be a rather simple problem, and used the above citation as verification of my solution. I'm not really sure how else to make it clear that I fully understand this problem, so I drew a graph to better visualize the invalidity of loops within a tree :)			

\end{homeworkProblem}


% Problem 5
\begin{homeworkProblem}
	The medici family was a powerful political dynasty and banking family in 15th century Florence. The classic network-based explanation of their power, offered by Padgett and Ansell in 1993, claims that they established themselves as the most central players within the network of prominent Florentine families.
	\textbf{a)} Construct a table with four columns, each containing a list of (family, score) pairs for the following centrality score functions:
	\begin{enumerate}
		\item degree centrality
		\item harmonic centrality (Eq. 7.30)
		\item eigenvector centrality (Eq. 7.6)
		\item betweenness centrality (Eq. 7.38)
	\end{enumerate}
			With each centrality, sort the pairs in decreasing order of importance; three decimal places is sufficient detail.\\
			Discuss (i) the degree to which these scores agree with Padgett and Ansell's claim that the Medicis occupied a structurally important position in this network, and (ii) what the scores say about the second most important family.
			
	\textbf{Solution:}\\
	Using each of the previously mentioned centrality measures, we can see that the Medici's are the most highly ranked family. Additionally, with a single exception, the Guadagni are consistently ranked the second most influential family.\\
	
	Also, it can be observed that the families Pucci, Pazzi, and Ginori are all consistently at the bottom of this ranking. Given these consistent observations for the provided network, Padgett and Ansell's claim that the Medici's occupied the most important position in the network appear to be valid.\\
	
	Observations:\\
	It is interesting that this dataset excludes certain prominent families such as the Borgia. I would be curious to see whether their inclusion in this network changes the results of this analysis.\\
	
	Question:\\
	The creation of a network such as this likely occurs over the course of years or even decades. Is it possible that the positioning of the Medici's success in banking resulted in their positioning within the network, rather than their position in the network being the source of their success?\\


	Note: The eigenvector  centrality equation from the textbook appears to be an iterative method where the initial centrality scores are all approximated to be equal to 1. The results under the eigenvector centrality column are the results obtained using only a single iteration. Additionally, it was concerning that the eigenvector centrality scores returned values greater than one in many cases. \\
	Given these observations, I have included networkx's eigenvector centrality scores in the Eigenvector NX Centrality column as an alternative measure.\\
	
	
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Problem5Table.jpg}\\
	
	\textbf{b)} Determine whether the relative structural importance of the Medicis can be explained solely by the degree sequence $\{k_i\}$ of the network G. To do this, use the configuration model to produce an ensemble of 1000 random graphs with the same degree sequence $\{k_i\}$ as G, and extract a distribution of harmonic centrality scores for each node. \\
	Make a figure showing the difference between a node's harmoinic centrality on G and its average harmonic centrality in the ensemble. On this figure, include lines showing the 25\% and 75\%  quantiles of the distribution around the mean for all vertices.\\
	Finally, (i) discuss your results here in terms of Padgett and Ansell's storyof the Medici family and (ii) comment on whether your results from part (5a) have changed.\\
	
	\textbf{Solution:}\\
		\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{random_degree_sequence.jpg}\\
	in order from left to right:\\
	Guadagni : 0\\
	Pazzi : 1\\
	Albizzi : 2\\
	Acciaiuoli : 3\\
	Strozzi : 4\\
	Pucci : 5\\
	Ginori : 6\\
	Peruzzi : 7\\
	Castellani : 8\\
	Ridolfi : 9\\
	Tornabuoni : 10\\
	Salviati : 11\\
	Bischeri : 12\\
	Medici : 13\\
	Barbadori : 14\\
	Lamberteschi : 15\\
	
	In the chart above, we can see that the average distance between the observed model and the configuration model fluctuates above and below a distance of 0, with a rather tight error bound. This indicates that the configuration model is a good approximation of the observed results, and enhances Padgett and Ansell's claim. \\
	Given the consistency with the configuration model, my conclusions from part a have not changed.\\
\end{homeworkProblem}

% Problem 6
\begin{homeworkProblem}
	Consider the task of designing a network in which distinct vertices hold the status of highest centrality for some set \textbf{S} of centrality measures. (Ties are prohibited). If the network is relatively small compared to the number of centrality measures, this can be a very difficult task.\\
	\begin{enumerate}
		\item As a warm up, design by hand a small network ($n \leq 7$) that has this property with respect to \textbf{S} = \{degree, betweenness\} centralities. Include a visualization of the network (label all the vertices, and indicate which ones win at which centrality measure) and produce a table in which the columns show the ranked vertices and scores for these centrality measures\\
		\textbf{Solution:}\\
		\includegraphics[keepaspectratio]{ec_graph.jpg}\\
		\textbf{Scores and corresponding nodes for Degree and Betweenness centrality measures, respectively}\\
		\includegraphics[keepaspectratio]{ec_degree.jpg}
		\includegraphics[keepaspectratio]{ec_between.jpg}\\
		
		\item Now, write a program to search the space of all possible connected, non-isomorphic networks of a given size n to find a network with this property for \textbf{S} = \{degree, harmonic, betweenness, eigenvector\} centralities, or report that no such network exists. Visualize that network, label all the vertices, and indicate which ones win at which centrality measure; and produce a table in which the columns show the ranked vertices and scores for these centrality measures. Comment briefly about what insights you gained about how to build networks that have distinct winners for different measures of centrality.
		
		\textbf{Solution:}\\
		Insert source of Frustration here... 
	\end{enumerate}
\end{homeworkProblem}

\pagebreak



\end{document}
% http://tuvalu.santafe.edu/~aaronc/courses/5352/csci5352_2017_L1.pdf

% http://www.journals.uchicago.edu/doi/pdfplus/10.1086/229693