import networkx as nx
import numpy as np
import random
import multiprocessing
import pickle

cur_iteration = 0

def create_planted_partition_graph(c, n, e, l):
    ## Parameters
    # c = average degree of nodes
    # n = number of nodes
    # e = epsilon = c_in - c_out
    # l = number of partitions
    k = n / l
    if (np.mod(k, 1) != 0):
        raise ValueError("n must be evenly divisible by l")
    p_in =  float(2 * c + e)/float(2 * n)
    p_out = float(2 * c - e)/float(2 * n)
    G = nx.planted_partition_graph(l=l, k=int(k), p_in=p_in, p_out=p_out)
    return G


def create_lattice_graph(width, height):
    '''
    :param width: width of the lattice graph
    :param height: height of the lattice graph
    :return: a list of edges to be added to the erdos-reyni graph.
    '''
    ctr = 0
    node_mat = []
    for i in range(width):
        row = []
        for j in range(height):
            row.append(ctr)
            ctr += 1
        node_mat.append(row)
    max_node = node_mat[-1][-1]
    # print(node_mat)
    G = nx.Graph()
    G.add_nodes_from(range(int(ctr)))

    for i in range(width):
        for j in range(height):
            # if i is greater than the minimum
            if i > 1:
                G.add_edge(node_mat[i][j], node_mat[i-1][j])
            if i < width - 2:
                G.add_edge(node_mat[i][j], node_mat[i+1][j])
            if j > 1:
                G.add_edge(node_mat[i][j], node_mat[i][j-1])
            if j < height - 2:
                G.add_edge(node_mat[i][j], node_mat[i][j+1])
    #
    # # TODO: This if statement may cause problems
    # if q != 0:
    #     for j in range(G.number_of_nodes()):
    #         neighbors = G.neighbors(j)
    #         new_nodes = [_k for _k in range(max_node + 1) if _k not in neighbors and _k != j]
    #         for pot_match in new_nodes:
    #             if random.random() > q:
    #                 G.add_edge(j, pot_match)

    return G




def initialize_graph(G):
    for i in range(G.number_of_nodes()):
        G.node[i]['infected'] = False
    for edge in G.edges():
        G.edges[edge]['visited'] = False
    return G


def count_infected(G):
    infected = 0
    for i in range(G.number_of_nodes()):
        if G.node[i]['infected'] == True:
            infected += 1
    return infected



def draw_infected_nodes(G):
    infected_nodes = []
    for node in G.nodes():
        if G.node[node]['infected'] == True:
            infected_nodes.append(node)
    immune_nodes = [i for i in range(G.number_of_nodes()) if i not in infected_nodes]
    pos = nx.spring_layout(G)
    nx.draw_networkx_nodes(G, pos, immune_nodes, node_color='b')
    nx.draw_networkx_nodes(G, pos, infected_nodes, node_color='g')
    nx.draw_networkx_labels(G, pos)
    nx.draw_networkx_edges(G, pos)


def infect(G, probability):
    # Identify infected nodes (Could identify newly infected nodes for efficiency)
    infected_nodes = []
    total_infections = []
    for node in G.nodes():
        if G.node[node]['infected'] == True:
            infected_nodes.append(node)
    for node in infected_nodes:
        edges = G.edges(node)
#         print("Edges " + str(edges))
        for edge in edges:
            if G.edges[edge]['visited'] == False:
                G.edges[edge]['visited'] = True
                rand_val = random.random()
                if rand_val < probability:
                    G.node[edge[1]]['infected'] = True
    # Check for new infections
    for node in G.nodes():
        if G.nodes[node]['infected'] == True:
            total_infections.append(node)
    return G, total_infections


def create_distance_graph(lattice_edges, n, q):
    G = nx.erdos_renyi_graph(n*n, q)
    for edge in lattice_edges:
        G.add_edge(edge[0], edge[1])
    return G


def main_1b(probability):
    n = 1000
    c = 8
    e = 0
    l = 2  # This doesnt matter since e = 0

    probability_values = []
    # Repeat 50 times and take the average.
    for j in range(500):
        G = create_planted_partition_graph(c, n, e, l)
        G = initialize_graph(G)
        unlucky_node = np.random.randint(n)
        G.node[unlucky_node]['infected'] = True
        previous_infected_count = -1
        current_infected_count = 1
        time = 0
        while current_infected_count > previous_infected_count:
            previous_infected_count = current_infected_count
            G, infections = infect(G, probability=probability)
            current_infected_count = len(infections)
            if current_infected_count > G.number_of_nodes():
                raise ValueError("The number of infected nodes exceeds the number of nodes")
            time += 1
        probability_values.append([time, (float(len(infections)) / float(G.number_of_nodes()))])

        avg_time = [_i[0] for _i in probability_values]
        avg_ratio = [_i[1] for _i in probability_values]

        avg_time = round(np.mean(avg_time), 4)
        avg_ratio = round(np.mean(avg_ratio), 4)
    return probability, avg_time, avg_ratio


def main_1c(args):
    # def main_1c(probability, epsilon):
    # global cur_iteration
    # cur_iteration += 1
    # print(cur_iteration)
    probability = args[0]
    epsilon = args[1]
    n = 200
    c = 8
    l = 2  # This doesnt matter since e = 0
    iter_data = []  # iter_data = [probability, end_time, infected_ratio]

    for k in range(200):
        G = create_planted_partition_graph(c, n, epsilon, l)
        G = initialize_graph(G)
        unlucky_node = np.random.randint(n)
        G.node[unlucky_node]['infected'] = True
        previous_infected_count = -1
        current_infected_count = 1
        time = 0
        #         iter_data.append([(i/prob_granularity), time, 1/G.number_of_nodes(), G.copy()])
        while current_infected_count > previous_infected_count:
            time += 1

            previous_infected_count = current_infected_count
            G, infections = infect(G, probability=probability)
            current_infected_count = len(infections)

        ratio = round((float(len(infections)) / float(G.number_of_nodes())), 4)
        iter_data.append([time, ratio, epsilon, probability])

    avg_time = [_i[0] for _i in iter_data]
    avg_ratio = [_i[1] for _i in iter_data]

    avg_time = round(np.mean(avg_time), 4)
    avg_ratio = round(np.mean(avg_ratio), 4)
    # iter_data.append([probability, e, avg_time, avg_ratio])
    return round(avg_time, 4), round(avg_ratio, 4), epsilon, probability


def main_2a(probability):
    n = 50
    # c = 8
    # e = 0
    # l = 2  # This doesnt matter since e = 0

    probability_values = []
    # Repeat 50 times and take the average.
    for j in range(250):
        # G = create_planted_partition_graph(c, n, e, l)
        G = create_lattice_graph(n, n)
        G = initialize_graph(G)
        unlucky_node = np.random.randint(n * n)
        G.node[unlucky_node]['infected'] = True
        previous_infected_count = -1
        current_infected_count = 1
        time = 0
        while current_infected_count > previous_infected_count:
            previous_infected_count = current_infected_count
            G, infections = infect(G, probability=probability)
            current_infected_count = len(infections)
            if current_infected_count > G.number_of_nodes():
                raise ValueError("The number of infected nodes exceeds the number of nodes")
            time += 1
        probability_values.append([time, (float(len(infections)) / float(G.number_of_nodes()))])

        avg_time = [_i[0] for _i in probability_values]
        avg_ratio = [_i[1] for _i in probability_values]

        avg_time = round(np.mean(avg_time), 4)
        avg_ratio = round(np.mean(avg_ratio), 4)
    return probability, avg_time, avg_ratio


def main_2b(args):
    probability = args[0]
    q = args[1]
    n = 25
    # c = 8
    # e = 0
    # l = 2  # This doesnt matter since e = 0
    F = create_lattice_graph(n, n)
    lattice_edges = F.edges()
    probability_values = []
    # Repeat 50 times and take the average.
    for j in range(25):
        # G = create_planted_partition_graph(c, n, e, l)
        G = create_distance_graph(lattice_edges, n, q)
        G = initialize_graph(G)
        unlucky_node = np.random.randint(n * n)
        G.node[unlucky_node]['infected'] = True
        previous_infected_count = -1
        current_infected_count = 1
        time = 0
        while current_infected_count > previous_infected_count:
            previous_infected_count = current_infected_count
            G, infections = infect(G, probability=probability)
            current_infected_count = len(infections)
            if current_infected_count > G.number_of_nodes():
                raise ValueError("The number of infected nodes exceeds the number of nodes")
            time += 1
        probability_values.append([time, (float(len(infections)) / float(G.number_of_nodes()))])
        del G

    avg_time = [_i[0] for _i in probability_values]
    avg_ratio = [_i[1] for _i in probability_values]

    avg_time = round(np.mean(avg_time), 4)
    avg_ratio = round(np.mean(avg_ratio), 4)
    del F
    return probability, avg_time, avg_ratio





if __name__ == "__main__":

    '''
    # Problem 1b
        probability_granularity = 1000

        probabilities = [float(_i)/float(probability_granularity) for _i in range(probability_granularity + 1)]

        print("Probabilities")
        print(probabilities)
        pool = multiprocessing.Pool()
        # results = pool.map(main_average_iter, probabilities)
        results = pool.map(main_1b,probabilities)

        print("Results")
        print(len(results))
        print(results)
        with open("placeholder.pkl", "wb") as f:
            pickle.dump(results, f)
            f.close()

        # iter_data = main_average_iter(.5)
        # print(iter_data)
    '''
    # Problem 1c
    probability_granularity = 100

    probabilities = [float(_i)/float(probability_granularity) for _i in range(probability_granularity + 1)]

    e_granularity = 12
    e_vals = [float(2 * 8) / float(e_granularity) * _i for _i in range(e_granularity + 1)]

    args = []
    for i in range(probability_granularity + 1):
        for j in range(e_granularity + 1):
            args.append((probabilities[i], e_vals[j]))

    print("Probabilities")
    print(probabilities)
    pool = multiprocessing.Pool()
    # results = pool.map(main_average_iter, probabilities)
    results = pool.map(main_1c, args)

    print("Results")
    print(len(results))
    print(results)
    with open("1c_final.pkl", "wb") as f:
        pickle.dump(results, f)
        f.close()

    iter_data = main_1c(.5)
    print(iter_data)

    '''
    Problem  2a
    probability_granularity = 20

    probabilities = [float(_i)/float(probability_granularity) for _i in range(probability_granularity + 1)]

    print("Probabilities")
    print(probabilities)
    pool = multiprocessing.Pool()
    # results = pool.map(main_average_iter, probabilities)
    results = pool.map(main_2a,probabilities)

    print("Results")
    print(len(results))
    print(results)
    with open("2a_data.pkl", "wb") as f:
        pickle.dump(results, f)
        f.close()

    '''
    #
    # probability_granularity = 15
    # q_granularity = 15
    #
    # probabilities = [float(_i)/float(probability_granularity) for _i in range(probability_granularity + 1)]
    # q_probabilities = [float(_i)/float(q_granularity) for _i in range(q_granularity + 1)]
    #
    # args = [] # probability, q
    # for i in range(probability_granularity + 1):
    #     for j in range(q_granularity + 1):
    #         args.append((probabilities[i], q_probabilities[j]))
    # print(args)
    # # print("Probabilities")
    # # print(probabilities)
    # pool = multiprocessing.Pool()
    # # results = pool.map(main_average_iter, probabilities)
    #
    # results = pool.map(main_2b, args)
    #
    # print("Results")
    # print(len(results))
    # print(results)
    # with open("2b_data.pkl", "wb") as f:
    #     pickle.dump(results, f)
    #     f.close()
