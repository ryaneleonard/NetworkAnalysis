import networkx as nx
import pandas as pd
import csv
import numpy as np
import random
import time
import pickle
import sys
import matplotlib.pyplot as plt

def choose_by_degree(G):
    tmp_edges = list(G.edges())
    idx = random.randint(0,len(tmp_edges)-1)
    chosen_node = tmp_edges[idx][0]
    return chosen_node


def preferential_attachment(G, a):
    c = 3
    new_node = G.number_of_nodes()

    if G.number_of_nodes == 1:
        print(degrees)
    r = random.random()
    if r < float(c) / float(c + a):
        chosen_node = choose_by_degree(G)
    else:
        chosen_node = random.randint(0, new_node - 1)
    G.add_node(new_node)
    G.add_edge(new_node, chosen_node)
    return(G)


def main(a):
    toc = time.time()
    # Create a 4 clique seed graph as in example
    G = nx.DiGraph()
    for i in range(4):
        G.add_node(i)
    for i in range(4):
        for j in range(4):
            if i != j:
                G.add_edge(i, j)
#     nx.draw(G)
    while G.number_of_nodes() < 1000000:
        if G.number_of_nodes() % 1000 == 0:
            print(G.number_of_nodes())
        G = preferential_attachment(G, a)
    print(time.time() - toc)
#     figure = plt.figure(figsize=(10, 10))
#     nx.draw(G)
    return G

if __name__ == "__main__":
    a = int(sys.argv[1])
    outfile = sys.argv[2]
    print("A: {}".format(a))
    print("Filename: {}".format(outfile))
    G = main(a)
    with open(outfile, 'wb') as f:
        pickle.dump(G, f)
    f.close()

    ## Read A pickled graph
    # outfile = sys.argv[1]
    # with open(outfile, 'rb') as f:
    #     new_G = pickle.load(f)
    #     f.close()
    # figure=plt.figure()
    # nx.draw(new_G, with_labels = True)
    # plt.show()
